api = "2"
core = "7.x"

; Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc4"

projects[block_class][subdir] = "contrib"
projects[block_class][version] = "1.3"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[clientside_validation][subdir] = "contrib"
projects[clientside_validation][version] = "1.x-dev"

projects[date][subdir] = "contrib"
projects[date][version] = "2.6"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.2"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.0"
projects[entityreference][patch][] = "http://drupal.org/files/1852112-entityreference-field-value-devel-generate.patch"

projects[fboauth][subdir] = "contrib"
projects[fboauth][version] = "1.6"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0"

projects[media][subdir] = "contrib"
projects[media][version] = "1.3"

projects[filefield_paths][subdir] = "contrib"
projects[filefield_paths][version] = "1.0-beta4"

projects[filefield_sources][subdir] = "contrib"
projects[filefield_sources][version] = "1.8"

projects[filefield_sources_plupload][subdir] = "contrib"
projects[filefield_sources_plupload][version] = "1.1"

projects[fivestar][subdir] = "contrib"
projects[fivestar][version] = "2.0-alpha2"

projects[flag][subdir] = "contrib"
projects[flag][version] = "2.1"

projects[internal_nodes][subdir] = "contrib"
projects[internal_nodes][version] = "1.2"
projects[internal_nodes][patch][] = "http://drupal.org/files/internal-nodes-node-view-1855310-2.patch"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.3"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[link][subdir] = "contrib"
projects[link][version] = "1.1"

projects[location][subdir] = "contrib"
projects[location][version] = "3.0-rc6"

projects[mailsystem][subdir] = "contrib"
projects[mailsystem][version] = "2.34"

projects[mediaelement][subdir] = "contrib"
projects[mediaelement][version] = "1.2"

projects[menu_token][subdir] = "contrib"
projects[menu_token][version] = "1.0-beta5"

projects[mimemail][subdir] = "contrib"
projects[mimemail][version] = "1.0-beta1"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.2"

projects[persistent_login][subdir] = "contrib"
projects[persistent_login][version] = "1.0-beta1"

projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.4"

projects[privatemsg][subdir] = "contrib"
projects[privatemsg][version] = "1.4"

projects[realname][subdir] = "contrib"
projects[realname][version] = "1.1"

projects[sharethis][subdir] = "contrib"
projects[sharethis][version] = "2.5"

projects[stringoverrides][subdir] = "contrib"
projects[stringoverrides][version] = "1.8"

projects[subpathauto][subdir] = "contrib"
projects[subpathauto][version] = "1.3"

projects[terms_of_use][subdir] = "contrib"
projects[terms_of_use][version] = "1.2"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"

projects[votingapi][subdir] = "contrib"
projects[votingapi][version] = "2.11"

; Themes
projects[omega][version] = "3.1"
projects[omega][patch][] = "https://drupal.org/files/relative-src-15.patch"

; Libraries
libraries[mediaelement][download][type] = "get"
libraries[mediaelement][download][url] = "https://github.com/johndyer/mediaelement/archive/master.zip"
libraries[mediaelement][directory_name] = "mediaelement"

libraries[jquery.history][download][type] = "git"
libraries[jquery.history][download][url] = "https://github.com/balupton/history.js.git"
libraries[jquery.history][directory_name] = "jquery.history"

libraries[jquery.mousewheel][download][type] = "git"
libraries[jquery.mousewheel][download][url] = "https://github.com/brandonaaron/jquery-mousewheel.git"
libraries[jquery.mousewheel][directory_name] = "jquery.mousewheel"

libraries[jquery.jscrollpane][download][type] = "git"
libraries[jquery.jscrollpane][download][url] = "https://github.com/vitch/jScrollPane.git"
libraries[jquery.jscrollpane][directory_name] = "jquery.jscrollpane"

libraries[jquery.tooltip][download][type] = "get"
libraries[jquery.tooltip][download][url] = "http://jquery.bassistance.de/tooltip/jquery.tooltip.zip"
libraries[jquery.tooltip][directory_name] = "jquery.tooltip"

libraries[plupload][download][type] = "get"
libraries[plupload][destination] = "libraries"
libraries[plupload][download][url] = "https://github.com/downloads/moxiecode/plupload/plupload_1_5_4.zip"
libraries[plupload][directory_name] = "plupload"
libraries[plupload][patch][] = "https://drupal.org/files/plupload-remove-examples-and-binaries-based-on-zip-2100581-4.patch"
