api = 2
includes[] = drupal-org-core.make

projects[totem][download][type] = git
projects[totem][download][url] = http://git.drupal.org/project/totem.git
projects[totem][download][branch] = 7.x-2.x
projects[totem][type] = profile
