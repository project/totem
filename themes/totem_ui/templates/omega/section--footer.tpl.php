<?php
/**
 * @file
 * section--footer.tpl.php
 */
?>

<div<?php print $attributes; ?>>
  <?php print $content; ?>
</div>
