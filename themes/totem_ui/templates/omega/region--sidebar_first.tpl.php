<?php
/**
 * @file
 * region--sidebar_first.tpl.php
 */
?>

<div<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php print $content; ?>
  </div>
</div>
