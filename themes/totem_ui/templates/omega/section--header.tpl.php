<?php
/**
 * @file
 * section--header.tpl.php
 */
?>

<div<?php print $attributes; ?>>
  <?php print $content; ?>
</div>
