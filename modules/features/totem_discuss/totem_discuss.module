<?php
/**
 * @file
 * totem_discuss.module
 */

require_once 'totem_discuss.features.inc';
require_once 'includes/totem_discuss.theme.inc';
require_once 'includes/totem_discuss.block.inc';
require_once 'includes/totem_discuss.form.inc';

// Private helper functions.
/**
 * TODO.
 */
function _totem_discuss_title_callback($title = NULL, $menu = NULL) {
  $count = privatemsg_unread_count();
  $return = t('Inbox');

  if ($count > 0) {
    $return = format_plural($count, 'Inbox <span class="count">1</span>', 'Inbox <span class="count">@count</span>');
  }

  return $return;
}
/**
 * Checks that a node type = community and return node_access result if TRUE.
 * PERMISSION.
 */
function _totem_discuss_menu_access_callback_op_comment($comment, $op = 'view', $node, $op_node_override) {

  global $user;
  
  $access_comment = comment_access($op, $comment);
  $access_node = node_access($op_node_override, $node);

  $access = ($access_comment || $access_node);

  // Reset reply access and rely exclusively on case below.
  if ($op == 'reply') {
    $access = FALSE;
  }

  // This exists because there's no hook_comment_access we can use for
  // custom privileges.
  if (!$access) {
    switch ($op) {
      case 'reply':
        $access = (_totem_user_has_community($user, $node) || user_access('administer comments'));
        break;
      case 'delete':
        if ($comment->uid == $user->uid && user_access('delete own comments')) {
          $access = TRUE;
        }
        break;
    }
  }

  return $access;
}
/**
 * TODO.
 */
function _totem_discuss_menu_access_callback_op_privatemsg($permission = 'read privatemsg', $account = NULL) {
  $grant = privatemsg_user_access($permission, $account);

  global $user;

  // Make sure current user can't see Inbox tab of user being viewed.
  if ($grant) {
    if (arg(0) == 'user') {
      $user_current = arg(1);
      $user_current = user_load($user_current);

      if ($user->uid !== $user_current->uid && !privatemsg_user_access('read all private messages', $user)) {
        $grant = FALSE;
      }
    }
  }

  return $grant;
}

// Hook implementations.
/**
 * Implements hook_permission().
 */
function totem_discuss_permission() {

  $perms = array(
    'delete own comments' => array(
      'title' => t('Delete own comments and all replies'),
    ),
  );

  return $perms;
}
/**
 * Implements hook_menu().
 */
function totem_discuss_menu() {

  // Callback to report abuse.
  $description = t('Report this content as abusive or inappropriate to the @community managers.', array('@community' => t('Community')));
  $report_args = array(
    'title' => 'Report abuse',
    'description' => $description,
    'file' => 'includes/totem_discuss.form.inc',
    'page callback' => 'totem_discuss_form_node',
    'page arguments' => array(3, 1, 4, 0),
    'access callback' => 'node_access',
    'access arguments' => array('view', 1),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'tab_parent' => 'node/%',
    'tab_root' => 'node/%',
  );
  $items['node/%node/modal/report/%totem_common_context_community'] = $report_args;
  $items['comment/%comment/modal/report/%node'] = $report_args;
  $items['comment/%comment/modal/report/%node']['access callback'] = '_totem_discuss_menu_access_callback_op_comment';
  $items['comment/%comment/modal/report/%node']['access arguments'] = array(1, 'view', 4, 'view');

  // Callback to write a new privatemsg.
  $items['user/%user/modal/message'] = array(
    'title' => 'New message',
    'description' => 'Send a private message to this member.',
    'file' => 'includes/totem_discuss.form.inc',
    'page callback' => 'totem_discuss_form_message',
    'page arguments' => array(1, 4, 5),
    'access callback' => 'privatemsg_user_access',
    'access arguments' => array('write privatemsg'),
    'type' => MENU_CALLBACK,
  );

  // Callback to reply to a comment (inline via AJAX).
  $items['comment/%comment/inline/reply/%node/%totem_common_context_community'] = array(
    'title' => 'Reply',
    'description' => 'Reply to this comment.',
    'file' => 'includes/totem_discuss.form.inc',
    'page callback' => 'totem_discuss_put_inline_comment_reply_form',
    // Args sig matches comment_reply().
    'page arguments' => array(4, 1),
    // Effective perm: if a user has a field_community match with this node,
    // allow comment reply.
    'access callback' => '_totem_discuss_menu_access_callback_op_comment',
    'access arguments' => array(1, 'reply', 4, 'view'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'delivery callback' => 'ajax_deliver',
  );

  // Callback to edit a comment (inline via AJAX).
  $items['comment/%comment/inline/edit/%node/%totem_common_context_community'] = array(
    'title' => 'Edit',
    'description' => 'Edit this comment.',
    'file' => 'includes/totem_discuss.form.inc',
    'page callback' => 'totem_discuss_put_inline_comment_edit_form',
    'page arguments' => array(1, 4),
    // Effective perm: if a user can edit this comment (i.e. author),
    // OR user can update current node, allow comment edit.
    'access callback' => '_totem_discuss_menu_access_callback_op_comment',
    'access arguments' => array(1, 'edit', 4, 'update'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'delivery callback' => 'ajax_deliver',
  );

  // Callback to delete a comment (inline via AJAX).
  $items['comment/%comment/inline/delete/%node/%totem_common_context_community'] = array(
    'title' => 'Delete',
    'description' => 'Delete this comment and all replies.',
    'file' => 'includes/totem_discuss.form.inc',
    'page callback' => 'totem_discuss_put_inline_comment_delete_form',
    'page arguments' => array(1),
    // Effective perm: if a user can edit this comment (i.e. author),
    // OR user can update current node, allow comment delete.
    'access callback' => '_totem_discuss_menu_access_callback_op_comment',
    'access arguments' => array(1, 'delete', 4, 'update'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'delivery callback' => 'ajax_deliver',
  );

  return $items;
}
/**
 * Implements hook_menu_alter().
 */
function totem_discuss_menu_alter(&$items) {

  // Remove top-level privatemsg lists;
  // We only want this accessible from the user's profile (user/%/messages).
  unset($items['messages/list']);

  // Alter user-menu item.
  $items['messages']['title callback'] = '_totem_discuss_title_callback';
  $items['messages']['title arguments'] = array('Messages', 'user-menu');
  unset($items['messages']['file']);

  // Force privatemsg tab to display for all users who can 'read privatemsg'.
  // Vs. 'read all private messages'.
  $items['user/%/messages']['weight'] = -47;
  $items['user/%/messages']['title callback'] = '_totem_discuss_title_callback';
  $items['user/%/messages']['access callback'] = '_totem_discuss_menu_access_callback_op_privatemsg';
  $items['user/%/messages']['access arguments'] = array('read privatemsg');


  // Remove comment permalinks...comments are associated with internal_node,
  // which could be associated to multiple parent community nodes. Also note
  // that by default, this module allows anonymous access to comments (in order
  // for comments to be indexed by search)...so we're also effectively
  // overriding that workaround here.
  // @see http://drupal.org/node/1396534
  unset($items['comment/%']);
  unset($items['comment/%/view']);

}
/**
 * Implements hook_menu_local_tasks_alter().
 */
function totem_discuss_menu_local_tasks_alter(&$data, $router_item, $root_path) {

  global $user;

  if (stripos($router_item['href'], 'user') === 0) {

    $item = menu_get_item("user/{$user->uid}/modal/message");
    $item['localized_options'] = array('attributes' => array('class' => array('btn', 'small', 'corners', 'ctools-use-modal', drupal_html_class($item['title']))));

    if ($user->uid !== arg(1)) {
      $item['href'] .= '/' . arg(1);
    }
    else {
      // Hide "New message" button on user's own overview screen.
      if (drupal_get_path_alias() == ltrim(url("user/{$user->uid}"), '/')) {
        $item['access'] = FALSE;
      }
    }

    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }

}
/**
 * Implements hook_contextual_links_view_alter().
 */
function totem_discuss_contextual_links_view_alter(&$element, $items) {

  global $user;

  if (!empty($element['#links']['node-modal-report-'])) {
    // Don't show "Report abuse" option in teaser mode.
    if ($element['#element']['#view_mode'] == 'teaser') {
      unset($element['#links']['node-modal-report-']);
    }
  }

  if (!empty($element['#element']['#comment'])) {
    $comment = $element['#element']['#comment'];
    $node_community = _totem_common_get_community_context_node();

    if (!empty($node_community)) {
      $paths = array(
        'reply' => 'comment/' . $comment->cid . '/inline/reply/' . $comment->nid . '/' . $node_community->nid,
        'message' => 'user/' . $user->uid . '/modal/message/' . $comment->uid,
        'edit' => 'comment/' . $comment->cid . '/inline/edit/' . $comment->nid . '/' . $node_community->nid,
        'delete' => 'comment/' . $comment->cid . '/inline/delete/' . $comment->nid . '/' . $node_community->nid,
        'report' => 'comment/'.$comment->cid.'/modal/report/'.$comment->nid.'',
      );

      foreach ($paths as $key => $path) {
        $item_menu = menu_get_item($path);
        if ($item_menu['access']) {
          $item_contextual = array(
            'title' => $item_menu['title'] . ' <span></span>',
            'href' => $path,
            'html' => TRUE,
          );

          if (strpos($path, '/inline/') !== FALSE) {
            // TODO: see how <a> elements with this class get processed in
            // drupal/misc/ajax.js: consider using an #ajax property which
            // will allow for specifying bunch more settings, like progress
            // element and effect. I think.
            $item_contextual['attributes']['class'][] = 'use-ajax';
            // At the very least (for now), should have an ID attr so that
            // all these links will not overwrite each other in the
            // Drupal.ajax array!
            $item_contextual['attributes']['id'] = 'comment-' . $comment->cid . '-' . $key . '-link';
          }

          $element['#links']['comment-' . $key] = $item_contextual;
        }
      }

      // TODO: for now, remove "subscribe_node" contextual Flag from comments;
      // cool implications, but not something we want to support quite yet.
      if (!empty($element['#links']['flag-subscribe_node'])) {
        unset($element['#links']['flag-subscribe_node']);
      }
    }
  }

  // Use menu item's description param for the contextual link's title attrib.
  // The jquery.tooltip plugin picks it up from there.
  foreach ($element['#links'] as $key => &$meta_link) {
    if (!empty($meta_link['href'])) {
      $item = menu_get_item($meta_link['href']);
      if (!empty($item['description'])) {
        $meta_link['attributes']['title'] = $item['description'];
      }
    }
  }
  // We're going to add the node's contextual links to this list of links.
  if (!empty($element['#element']['links']['#links'])) {
    foreach ($element['#element']['links']['#links'] as $link_key => $link_val) {
      $element['#links'][$link_key] = $link_val;
    }
    // I think if we unset here, it will still be rendered at the node level.
    unset($element['#element']['links']['#links']);
  }

}
/**
 * Implements hook_url_outbound_alter().
 */
function totem_discuss_url_outbound_alter(&$path, &$options, $original_path) {
  _totem_common_url_outbound_alter($path, $options, $original_path, 'topic', 'topics');

  global $user;

  // Reroute request for /messages to user's local inbox tab.
  if ($path == 'messages') {
    $path = "user/{$user->uid}/messages";
  }

}
/**
 * Implements hook_theme_registry_alter().
 */
function totem_discuss_theme_registry_alter(&$theme_registry) {
  $path = drupal_get_path('module', 'totem_discuss');
  _totem_common_theme_registry_alter($theme_registry, $path);
}
/**
 * Implements hook_node_view().
 */
function totem_discuss_node_view($node, $view_mode) {

  global $user;

  // Override comment mod's behavior.
  // @see comment_node_view()
  if ($view_mode == 'full' && empty($node->in_preview)) {

    // Append comments regardless of page context...we only care
    // that the "full" view mode is active.
    //
    // Init pager to make sure that the comment list is handled independently
    // of any other theme pager calls. Without this, the "default entity"
    // scenario in totem_common_node_community is incorrectly outputting the
    // pager when other paged queries are running elsewhere. Ultimately the
    // result of ridiculous global vars in core paging subsystem.
    pager_default_initialize($node->comment_count, variable_get('comment_default_per_page_' . $node->type, 50));
    $node->content['comments'] = comment_node_page_additions($node);

    // Force node comment links to use altered, contextual paths.
    if ($node->comment !== COMMENT_NODE_HIDDEN) {
      if (!empty($node->content['links']['comment'])) {
        foreach ($node->content['links']['comment']['#links'] as $key => &$meta) {
          $path = ltrim(url('node/' . $node->nid, array(
            'entity_type' => 'node',
            'entity' => $node,
          )), '/');

          $meta['href'] = $path;
        }
      }
    }

  }

  // Alter privatemsg link.
  if (!empty($node->content['links']['#links']['privatemsg_link'])) {
    $node->content['links']['#links']['privatemsg_link']['title'] = t('New message');
    $node->content['links']['#links']['privatemsg_link']['href'] = 'user/' . $user->uid . '/modal/message/' . $node->uid;
  }

}
/**
 * Implements hook_comment_view().
 */
function totem_discuss_comment_view($comment) {

  // Trigger contextual wrapper stuff.
  // Links are actually added in @totem_discuss_contextual_links_view_alter.
  $comment->content['#contextual_links']['comment'] = array('comment', array($comment->cid));

}
/**
 * Implements hook_comment_view_alter().
 */
function totem_discuss_comment_view_alter(&$build) {

  $comment = &$build['#comment'];

  // Remove the anchor element so we can give the comment div the 'comment-$cid' ID.
  $anchor = "<a id=\"comment-{$comment->cid}\"></a>\n";
  $build['#prefix'] = str_replace($anchor, "", $build['#prefix']);
  $build['#comment_div_id'] = TRUE;

  // Add an ID to indentation divs so we can append a reply within them.
  if ($comment->pid) {
    $build['#prefix'] = str_replace('<div class="indented">', '<div id="sub-comment-' . $comment->pid . '" class="indented">', $build['#prefix']);
  }

  // Attach user's sized images to comment object.
  $account = user_load($comment->uid);
  $account = user_view($account, 'teaser');
  if (!empty($account['#account']->content)) {
    $comment->content['images'] = $account['#account']->content['images'];
  }

}
/**
 * Implements hook_privatemsg_operation_executed().
 */
function totem_discuss_privatemsg_operation_executed($operation, $threads, $account = NULL) {

  global $user;

  // Check if that operation has defined an undo callback.
  if (isset($operation['undo callback']) && $undo_function = $operation['undo callback']) {
    // If an undo callback was defined, remove the last status message that
    // provides the "Undo" link. Instead set message destination param to
    // return to user/uid/messages path.
    unset($_SESSION['messages']['status'][1]);

    $undo_message = t('The previous action can be <a href="!undo">undone</a>.', array(
      '!undo' => url('messages/undo/action', array(
        'query' => array(
          'destination' => "user/{$user->uid}/messages",
        ),
      )),
    ));

    drupal_set_message($undo_message);
  }

}
/**
 * Implements hook_privatemsg_recipient_type_info_alter().
 */
function totem_discuss_privatemsg_recipient_type_info_alter(&$types) {

  $types['user']['description'] = '';

}
/**
 * Implements hook_query_TAG_alter().
 */
function totem_discuss_query_NODES_LAST_COMMENTED_alter(QueryAlterableInterface $query) {
  // Note: it is up to any function that adds this tag to ensure
  // that the entity_type being queried is 'node' so that this join
  // makes any sense in the first place.
  $query->innerJoin('node_comment_statistics', 'ncs', 'ncs.nid = node.nid');
  // We only want nodes that have comments.
  $query->condition('ncs.comment_count', 0, '>');
  $query->orderBy('ncs.last_comment_timestamp', 'DESC');
}

// Hook implementations (Features).
/**
 * Implements hook_node_community_ENTITY_TYPE().
 */
function totem_discuss_node_community_topic($vars) {

  $efq_params = array(
    'entity_type' => 'node',
    'bundle' => 'topic',
    'field_conditions' => array(
      array('field' => 'field_community', 'column' => 'target_id', 'value' => $vars['node']->nid),
    ),
    'page_limit' => (!empty($vars['page_limit']) ? $vars['page_limit'] : NULL),
    'property_order_by' => array(
      array('column' => 'changed', 'direction' => 'DESC'),
    ),
  );

  $efq_params = array_merge($efq_params, $vars);

  $items['nodes'] = array(
    'query' => _totem_common_efq($efq_params),
  );

  return $items;
}
/**
 * Implements hook_node_community_ENTITY_TYPE_entity().
 */
function totem_discuss_node_community_topic_entity($vars) {

  $efq_params = array(
    'view_mode' => 'full',
    'property_conditions' => array(
      array('column' => 'nid', 'value' => $vars['entity_id']),
    ),
  );

  $efq_params = array_merge($efq_params, $vars);

  return module_invoke('totem_discuss', 'node_community_topic', $efq_params);
}
/**
 * Implements hook_user_community_ENTITY_TYPE().
 */
function totem_discuss_user_community_topic($vars) {

  // Get user's list of active Community nids.
  $nid_community = _totem_common_get_field_entityreference_values('user', $vars['account'], 'field_community');
  if (empty($nid_community)) {
    return NULL;
  }

  $efq_params = array(
    'entity_type' => 'node',
    'bundle' => 'topic',
    'property_conditions' => array(
      array('column' => 'uid', 'value' => $vars['account']->uid),
    ),
    'field_conditions' => array(
      array('field' => 'field_community', 'column' => 'target_id', 'value' => $nid_community),
    ),
    'page_limit' => (!empty($vars['page_limit']) ? $vars['page_limit'] : PAGE_SIZE_LISTS_PAGE),
    'property_order_by' => array(
      array('column' => 'changed', 'direction' => 'DESC'),
    ),
  );

  $items['nodes'] = array(
    'query' => _totem_common_efq($efq_params),
  );

  return $items;
}
/**
 * Implements hook_user_community_form_submit().
 */
function totem_discuss_user_community_form_submit($vars) {
  $op = $vars['op'];
  $node = $vars['node'];
  $account = $vars['account'];
  $form_state = (!empty($vars['form_state']) ? $vars['form_state'] : NULL);

  $recipients = array();
  $subject = '';
  $body = '';


  // IMPORTANT: Message body is shared between sender and recipients...
  // So don't allow copy, links, etc. here that is role-centric.
  switch ($op) {
    case 'request_unblock':
      // Send to managers.
      $managers = _totem_user_get_managers($node);
      $recipients = user_load_multiple($managers);
      $subject = t('"@title" - unblock request', array('@title' => $node->title));
      $body = t("<p>!name has requested to be unblocked from !title, which you manage. !action to approve this member's request and join them to the @community.</p>", array(
        '!name' => l($account->realname, 'user/' . $account->uid, array('absolute' => TRUE)),
        '!title' => l($node->title, 'node/' . $node->nid . '/members', array('absolute' => TRUE)),
        '!action' => l(t('Visit the blocked members page'), "node/{$node->nid}/members", array(
          'absolute' => TRUE,
          'query' => array(
            'status' => 'blocked',
          ),
        )),
        '@community' => t('community'),
      ));
      $body .= t('<p>@name wrote:</p>', array('@name' => $account->realname));
      $body .= $form_state['values']['message'];
      break;

    case 'join':
      // Send to managers.
      // Only bother sending if this is not an Open community.
      $node_entity = entity_metadata_wrapper('node', $node);
      if ($node_entity->field_community_status->value() != TOTEM_COMMUNITY_STATUS_OPEN) {

        $managers = _totem_user_get_managers($node);
        $recipients = user_load_multiple($managers);
        $subject = t('"@title" - join request', array('@title' => $node->title));
        $body = t("<p>!name has requested to join !title, which you manage. !action to approve this member's request.</p>", array(
          '!name' => l($account->realname, "user/{$account->uid}", array('absolute' => TRUE)),
          '!title' => l($node->title, "node/{$node->nid}", array('absolute' => TRUE)),
          '!action' => l(t('Visit the pending members page'), "node/{$node->nid}/members", array(
            'absolute' => TRUE,
            'query' => array(
              'status' => 'pending',
            ),
          )),
        ));
        $body .= t('<p>@name wrote:</p>', array('@name' => $account->realname));
        $body .= $form_state['values']['message'];
      }
      break;

    case 'invite':
      // Do nothing; handled by totem_user directly.
      break;

    case 'leave':
      // Do nothing.
      break;

    case 'remove':
      // Send to account.
      $recipients[] = $account;
      $subject = t('"@title" - removal', array('@title' => $node->title));
      $body = t('<p>You have been removed from !title.</p>', array(
        '!title' => l($node->title, 'node/' . $node->nid, array('absolute' => TRUE)),
      ));
      break;

    case 'block':
      // Do nothing.
      // $recipients[] = $account;
      // $subject = t('"@title" - blocked', array('@title' => $node->title));
      // $body = t('<p>You have been blocked from !title.</p>', array(
      // '!title' => l($node->title, 'node/' . $node->nid, array('absolute' => TRUE)),
      // ));
      break;

    case 'permissions':
      // Send to account.
      $recipients[] = $account;
      $subject = t('"@title" - permissions', array('@title' => $node->title));
      $body = t('<p>Your permissions in !title have been changed.</p>', array(
        '!title' => l($node->title, 'node/' . $node->nid, array('absolute' => TRUE)),
      ));
      break;

    case 'approve':
      // Send to account.
      $recipients[] = $account;
      $subject = t('"@title" - join request approved', array('@title' => $node->title));
      $body = t('<p>Your request to join !title has been approved.</p>', array(
        '!title' => l($node->title, 'node/' . $node->nid, array('absolute' => TRUE)),
      ));
      break;

    case 'unblock':
      // Send to account.
      $recipients[] = $account;
      $subject = t('"@title" - unblock request approved', array('@title' => $node->title));
      $body = t('<p>Your request to join !title has been approved.</p>', array(
        '!title' => l($node->title, 'node/' . $node->nid, array('absolute' => TRUE)),
      ));
      break;
  }

  if (!empty($recipients) && !empty($subject)) {

    $message = privatemsg_new_thread($recipients, $subject, $body);
    if ($message['message'] !== FALSE && $message['success'] == TRUE) {
      // drupal_set_message( print_r($message, TRUE) );
    }

    foreach ($message['messages'] as $type => $group) {
      foreach ($group as $m) {
        drupal_set_message(check_plain($m), $type);
      }
    }
  }

}
